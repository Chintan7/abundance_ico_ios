//
//  ChangePINViewController.swift
//  Native Wallet
//
//  Created by Solulab_Mosam on 04/07/18.
//  Copyright © 2018 Solulab_Mosam. All rights reserved.
//

import UIKit

class ChangePINViewController: BaseViewController,UIGestureRecognizerDelegate
{

    @IBOutlet var pinView:SVPinView!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Change PIN"
        
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(self.btnBackPress), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        self.view.layerGradient()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        tap.delegate = self
        self.view.addGestureRecognizer(tap)
    }
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
       self.view.endEditing(true)
    }
    @objc func btnBackPress() {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    @IBAction func btnContinuePress(sender:UIButton)
    {
        self.verifyCode()
    }
    func verifyCode() {
        let pin = self.pinView.getPin()
        print(pin)
        guard !pin.isEmpty else
        {
            UtilityClass.showAlert("Pin entry incomplete.")
//            self.showAlert(title: "Error", message: "Pin entry incomplete")
            return
        }
    }
    @IBAction func btnResetPINPress(sender:UIButton)
    {
        let verifyPINObj = self.storyboard?.instantiateViewController(withIdentifier: "VerifyOTPViewController") as! VerifyOTPViewController
        self.navigationController?.pushViewController(verifyPINObj, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
