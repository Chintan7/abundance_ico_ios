//
//  SettingsViewController.swift
//  Native Wallet
//
//  Created by Solulab_Mosam on 03/07/18.
//  Copyright © 2018 Solulab_Mosam. All rights reserved.
//

import UIKit

class SettingsViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet var tblObj : UITableView!
    @IBOutlet var tblCell1 : UITableViewCell!
    @IBOutlet var tblCell2 : UITableViewCell!
    @IBOutlet var tblCell3 : UITableViewCell!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.view.layerGradient()
        
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Settings"
        
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "menu"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        tblObj.tableFooterView = UIView()
    }
     
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.row == 0
        {
            tblCell1.selectionStyle = .none
            return tblCell1
        }
        else if indexPath.row == 1
        {
            tblCell2.selectionStyle = .none
            return tblCell2
        }
        else
        {
            tblCell3.selectionStyle = .none
            return tblCell3
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 2
        {
            let changePinViewObj = self.storyboard?.instantiateViewController(withIdentifier: "ChangePINViewController") as! ChangePINViewController
            self.navigationController?.pushViewController(changePinViewObj, animated: true)
        }
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
