//
//  WalletViewController.swift
//  Native Wallet
//
//  Created by Solulab_Mosam on 04/07/18.
//  Copyright © 2018 Solulab_Mosam. All rights reserved.
//

import UIKit

class WalletViewController: BaseViewController
{

    override var prefersStatusBarHidden: Bool
    {
        return false
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "Wallet"
        
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "menu"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        self.view.layerGradient()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
