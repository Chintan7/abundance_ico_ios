//
//  BaseViewController.swift
//  AKSwiftSlideMenu
//
//  Created by Ashish on 21/09/15.
//  Copyright (c) 2015 Kode. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController, SlideMenuDelegate
{
//    var deleg : EndEditiingDelegate!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func slideMenuItemSelectedAtIndex(_ index: Int32) {
        let topViewController : UIViewController = self.navigationController!.topViewController!
        print("View Controller is : \(topViewController) \n", terminator: "")
        
        switch(index)
        {
        case 0:
            self.openViewControllerBasedOnIdentifier("WalletViewController")
            break
//        case 1:
//            self.openViewControllerBasedOnIdentifier("TradeViewController")
//            break
        case 1:
            self.openViewControllerBasedOnIdentifier("MyAddressViewController")
            break
        case 2:
            self.openViewControllerBasedOnIdentifier("StatementViewController")
            break
        case 3:
            self.openViewControllerBasedOnIdentifier("EarnReferralViewController")
            break
        case 4:
            self.openViewControllerBasedOnIdentifier("KYCVerificationViewController")
            
            break
        case 5:
            self.openViewControllerBasedOnIdentifier("HelpViewController")
            break
        case 6:
            self.openViewControllerBasedOnIdentifier("AboutUsViewController")
            break
        default:
            print("default\n", terminator: "")
        }
    }
    
    func logout() {
        let alertController = UIAlertController(title: APP_NAME, message: "Are you sure you want to logout?", preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "YES", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in           
        }
        let DestructiveAction = UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
            print("Destructive")
        }
        alertController.addAction(okAction)
        
        alertController.addAction(DestructiveAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func openViewControllerBasedOnIdentifier(_ strIdentifier:String) {
        let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: strIdentifier)
        self.navigationController!.pushViewController(destViewController, animated: false)
    }
    
//    func addSlideMenuButton()
//    {
//        let btnShowMenu = UIButton(type: UIButtonType.system)
//        btnShowMenu.setImage(#imageLiteral(resourceName: "menu"), for: UIControlState())
//        btnShowMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
//        btnShowMenu.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
//        let customBarItem = UIBarButtonItem(customView: btnShowMenu)
//        self.navigationItem.leftBarButtonItem = customBarItem;
//    }

    func defaultMenuImage() -> UIImage {
        var defaultMenuImage = UIImage()
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 22, height: 22), false, 0.0)
        
        UIColor.black.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 3, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 10, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 17, width: 30, height: 1)).fill()
        
        UIColor.white.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 4, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 11,  width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 18, width: 30, height: 1)).fill()
        
        defaultMenuImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
       
        return defaultMenuImage;
    }
    
    @objc func onSlideMenuButtonPressed(_ sender : UIButton) {
         let topViewController : UIViewController = self.navigationController!.topViewController!
        
        topViewController.view.endEditing(true)
        if (sender.tag == 10)
        {
            self.slideMenuItemSelectedAtIndex(-1);
            
            sender.tag = 0;
            
            let viewMenuBack : UIView = view.subviews.last!
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
                }, completion: { (finished) -> Void in
                    viewMenuBack.removeFromSuperview()
//                    self.navigationItem.rightBarButtonItem?.isEnabled = true
            })
            
            return
        }
        
        sender.isEnabled = false
        sender.tag = 10
        
        let menuVC : MenuViewController = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        menuVC.btnMenu = sender
        menuVC.delegate = self
        self.view.addSubview(menuVC.view)
        self.addChildViewController(menuVC)
        menuVC.view.layoutIfNeeded()
      //*******************************-
//        menuVC.navigationItem.title = "menu"
//        self.navigationItem.title = ""
        

        
        menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            sender.isEnabled = true
            
        }, completion:{ (finished) -> Void in
            menuVC.btnCloseMenuOverlay.backgroundColor = UIColor.black
            menuVC.btnCloseMenuOverlay.alpha = 0.3
//            self.navigationItem.rightBarButtonItem?.isEnabled = false

        })
    }
}
