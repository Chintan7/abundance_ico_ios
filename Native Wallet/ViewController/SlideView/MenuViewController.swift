//
//  MenuViewController.swift
//  AKSwiftSlideMenu
//
//  Created by Ashish on 21/09/15.
//  Copyright (c) 2015 Kode. All rights reserved.
//

import UIKit

protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ index : Int32)
}

class MenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    /**
    *  Array to display menu options
    */
    @IBOutlet var tblMenuOptions : UITableView!
    
    /**
    *  Transparent button to hide menu
    */
    @IBOutlet var btnCloseMenuOverlay : UIButton!
    
    /**
    *  Array containing menu options
    */
    var arrayMenuOptions = [Dictionary<String,String>]()
    
    /**
    *  Menu button which was tapped to display the menu
    */
    var btnMenu : UIButton!
    @IBOutlet var lblUserName : UILabel!
    @IBOutlet var imgProfile : UIImageView!
    
    /**
    *  Delegate of the MenuVC
    */
    var delegate : SlideMenuDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblMenuOptions.tableFooterView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.tblMenuOptions.frame.size.width, height: 1))
        lblUserName.text = "Wallet User"
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationItem.rightBarButtonItem = nil
        self.navigationItem.title = "SideMenu"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
      self.navigationItem.title = "SideMenu"
        updateArrayMenuOptions()
    }
    
    func updateArrayMenuOptions() {
        arrayMenuOptions.append(["title":"Wallet", "icon":"wallet"])
//        arrayMenuOptions.append(["title":"Trade", "icon":"trade"])
        arrayMenuOptions.append(["title":"My Address", "icon":"myaddress"])
        arrayMenuOptions.append(["title":"Statement", "icon":"statement"])
        arrayMenuOptions.append(["title":"Referral Rewards", "icon":"referral_white"])
        arrayMenuOptions.append(["title":"KYC", "icon":"KYC"])
        
        arrayMenuOptions.append(["title":"Help & Support", "icon":"help"])
        arrayMenuOptions.append(["title":"About Us", "icon":"support"])

        tblMenuOptions.reloadData()
    }   
    
    @IBAction func onProfileClick(_ button:UIButton!) {
        let SettingObj = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileViewController") as! MyProfileViewController
        self.navigationController?.pushViewController(SettingObj, animated: true)
//        base.slideMenuItemSelectedAtIndex(-1);
        
        //        sender.tag = 0;
//        onCloseMenuClick(button)
//        let isLogin = UserDefaults.standard.bool(forKey: "isLogin") as Bool
//        
//        if isLogin == true
//        {
//            let profileViewObj = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileViewController") as! MyProfileViewController
//            self.navigationController?.pushViewController(profileViewObj, animated: true)
//        }
//        else
//        {
//            let LoginViewObj = self.storyboard?.instantiateViewController(withIdentifier: "LoginviewController") as! LoginviewController
//            self.navigationController?.pushViewController(LoginViewObj, animated: true)
//        }
    }
    @IBAction func onSettinsClick(_ button:UIButton!) {
        let SettingObj = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        self.navigationController?.pushViewController(SettingObj, animated: true)
    }
    @IBAction func onLogoutClick(_ button:UIButton!) {
       
    }
    @IBAction func onNotificationClick(_ button:UIButton!) {
        let SettingObj = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(SettingObj, animated: true)
    }
    @IBAction func onCloseMenuClick(_ button:UIButton!) {
        btnMenu.tag = 0
        
        if (self.delegate != nil) {
            var index = Int32(button.tag)
            if(button == self.btnCloseMenuOverlay)
            {
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex(index)
        }
       
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
            self.btnCloseMenuOverlay.backgroundColor = UIColor.clear
            self.btnCloseMenuOverlay.alpha = 1
            }, completion: { (finished) -> Void in
                self.navigationItem.rightBarButtonItem?.isEnabled = true;
                self.view.removeFromSuperview()
                self.removeFromParentViewController()
        })
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellMenu")!
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.layoutMargins = UIEdgeInsets.zero
        cell.preservesSuperviewLayoutMargins = false
        cell.backgroundColor = UIColor.clear
        
        let lblTitle : UILabel = cell.contentView.viewWithTag(101) as! UILabel
        let imgIcon : UIImageView = cell.contentView.viewWithTag(100) as! UIImageView
        
        imgIcon.image = UIImage(named: arrayMenuOptions[indexPath.row]["icon"]!)
        lblTitle.text = arrayMenuOptions[indexPath.row]["title"]!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let btn = UIButton(type: UIButtonType.custom)
        btn.tag = indexPath.row
        self.onCloseMenuClick(btn)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMenuOptions.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
}
