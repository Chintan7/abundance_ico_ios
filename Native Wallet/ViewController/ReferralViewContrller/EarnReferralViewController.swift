//
//  EarnReferralViewController.swift
//  Native WSystemet
//
//  Created by Solulab_Mosam on 02/07/18.
//  Copyright © 2018 Solulab_Mosam. System rights reserved.
//

import UIKit

class EarnReferralViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate
{
    
    @IBOutlet var tblPolicy : UITableView!

    @IBOutlet var tblObj : UITableView!
    
    @IBOutlet var lblSystem : UILabel!
    @IBOutlet var lblSystemLine : UILabel!
    @IBOutlet var lblToken : UILabel!
    @IBOutlet var lblTokenLine : UILabel!
    @IBOutlet var lblPolicy : UILabel!
    @IBOutlet var lblPolicyLine : UILabel!
    
    var isSystem : Bool! = true
    var isToken : Bool! = false
    var isPolicy : Bool! = false
    
    @IBOutlet var viewSystem : UIView!
    @IBOutlet var viewToken : UIView!
    @IBOutlet var scrPolicy : UIScrollView!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.view.layerGradient()
        
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Earn Referral Rewards"
        
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "menu"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        selectSystem()
    }
    @IBAction func btnSystemPress(sender:UIButton)
    {
        selectSystem()
    }
    @IBAction func btnTokenPress(sender:UIButton)
    {
        selectToken()
        tblObj.reloadData()

    }
    
    @IBAction func btnPolicyPress(sender:UIButton)
    {
        selectPolicy()
        tblPolicy.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == tblObj
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! referralListCell
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellPolicy") as! referralListCell
            return cell
        }
    }
    func selectSystem()
    {
        isSystem = true
        isPolicy = false
        isToken = false
        lblSystem.textColor = GradientColor1
        lblSystemLine.backgroundColor = GradientColor1
        lblToken.textColor = GradientColor1.withAlphaComponent(0.7)
        lblTokenLine.backgroundColor = GradientColor1.withAlphaComponent(0.7)
        lblPolicy.textColor = GradientColor1.withAlphaComponent(0.7)
        lblPolicyLine.backgroundColor = GradientColor1.withAlphaComponent(0.7)
        
        viewSystem.isHidden = false
        viewToken.isHidden = true
        scrPolicy.isHidden = true
    }
    func selectToken()
    {
        isSystem = false
        isPolicy = false
        isToken = true
        lblSystem.textColor = GradientColor1.withAlphaComponent(0.7)
        lblSystemLine.backgroundColor = GradientColor1.withAlphaComponent(0.7)
        lblToken.textColor = GradientColor1
        lblTokenLine.backgroundColor = GradientColor1
        lblPolicy.textColor = GradientColor1.withAlphaComponent(0.7)
        lblPolicyLine.backgroundColor = GradientColor1.withAlphaComponent(0.7)
        
        viewSystem.isHidden = true
        viewToken.isHidden = false
        scrPolicy.isHidden = true
    }
    func selectPolicy()
    {
        isSystem = false
        isPolicy = true
        isToken = false
        lblSystem.textColor = GradientColor1.withAlphaComponent(0.7)
        lblSystemLine.backgroundColor = GradientColor1.withAlphaComponent(0.7)
        lblToken.textColor = GradientColor1.withAlphaComponent(0.7)
        lblTokenLine.backgroundColor = GradientColor1.withAlphaComponent(0.7)
        lblPolicy.textColor = GradientColor1
        lblPolicyLine.backgroundColor = GradientColor1
        
        viewSystem.isHidden = true
        viewToken.isHidden = true
        scrPolicy.isHidden = false
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
class referralListCell: UITableViewCell {
    
}
