//
//  StatementViewController.swift
//  Native Wallet
//
//  Created by Solulab_Mosam on 02/07/18.
//  Copyright © 2018 Solulab_Mosam. All rights reserved.
//

import UIKit

class StatementViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate
{
   
    
    @IBOutlet var tblObj : UITableView!
    @IBOutlet var lblAll : UILabel!
    @IBOutlet var lblAllLine : UILabel!
    @IBOutlet var lblWeek : UILabel!
    @IBOutlet var lblWeekLine : UILabel!
    @IBOutlet var lblMonth : UILabel!
    @IBOutlet var lblMonthLine : UILabel!

    var isAll : Bool! = true
    var isWeek : Bool! = false
    var isMonth : Bool! = false
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Account Statement"
        
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "menu"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        self.view.layerGradient()
        
        selectAll()
    }
    @IBAction func btnAllPress(sender:UIButton)
    {
        selectAll()
        tblObj.reloadData()
    }
    @IBAction func btnWeekPress(sender:UIButton)
    {
        selectWeek()
        tblObj.reloadData()
    }
    
    @IBAction func btnMonthPress(sender:UIButton)
    {
        selectMonth()
        tblObj.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! statementCell
        return cell
    }
    func selectAll()
    {
        isAll = true
        isMonth = false
        isWeek = false
        lblAll.textColor = GradientColor1
        lblAllLine.backgroundColor = GradientColor1
        lblWeek.textColor = GradientColor1.withAlphaComponent(0.7)
        lblWeekLine.backgroundColor = GradientColor1.withAlphaComponent(0.7)
        lblMonth.textColor = GradientColor1.withAlphaComponent(0.7)
        lblMonthLine.backgroundColor = GradientColor1.withAlphaComponent(0.7)
    }
    func selectWeek()
    {
        isAll = false
        isMonth = false
        isWeek = true
        lblAll.textColor = GradientColor1.withAlphaComponent(0.7)
        lblAllLine.backgroundColor = GradientColor1.withAlphaComponent(0.7)
        lblWeek.textColor = GradientColor1
        lblWeekLine.backgroundColor = GradientColor1
        lblMonth.textColor = GradientColor1.withAlphaComponent(0.7)
        lblMonthLine.backgroundColor = GradientColor1.withAlphaComponent(0.7)
    }
    func selectMonth()
    {
        isAll = false
        isMonth = true
        isWeek = false
        lblAll.textColor = GradientColor1.withAlphaComponent(0.7)
        lblAllLine.backgroundColor = GradientColor1.withAlphaComponent(0.7)
        lblWeek.textColor = GradientColor1.withAlphaComponent(0.7)
        lblWeekLine.backgroundColor = GradientColor1.withAlphaComponent(0.7)
        lblMonth.textColor = GradientColor1
        lblMonthLine.backgroundColor = GradientColor1
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
class statementCell: UITableViewCell {
    
}
