//
//  ForgotPasswordViewController.swift
//  Native Wallet
//
//  Created by Solulab_Mosam on 22/06/18.
//  Copyright © 2018 Solulab_Mosam. All rights reserved.
//

import UIKit
protocol ForgotPasswordDelegate
{
    func SendButtonClicked(_ secondDetailViewController: ForgotPasswordViewController)
}
class ForgotPasswordViewController: UIViewController {
    var delegate: ForgotPasswordDelegate?
    @IBOutlet var txtEmail : customTextField!
    @IBOutlet var lblError : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblError.isHidden = true

    }
    @IBAction func SendButtonClicked(sender:UIButton) {
        if validate() {
            delegate?.SendButtonClicked(self)
        }
    }
    func validate() -> Bool {
        if txtEmail.text?.count != 0
        {
            if UtilityClass.emailvalidate(txtEmail.text)
            {
                txtEmail.hideError()
                lblError.isHidden = true
                return true
            } else {
                txtEmail.showError()
                lblError.text = "Please enter valid email address."
                lblError.isHidden = false
                return false
            }
        } else {
            txtEmail.showError()
            lblError.text = "Please enter email address."
            lblError.isHidden = false
            return false
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
