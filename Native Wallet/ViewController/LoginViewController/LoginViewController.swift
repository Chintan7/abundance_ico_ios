//
//  LoginViewController.swift
//  Native Wallet
//
//  Created by Solulab_Mosam on 21/06/18.
//  Copyright © 2018 Solulab_Mosam. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController,ForgotPasswordDelegate {
    @IBOutlet var containerView : UIView!
    @IBOutlet var txtEmail : customTextField!
    @IBOutlet var txtPassword : customTextField!
    @IBOutlet var lblError : UILabel!
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.view.layerGradient()
        
        containerView.layer.cornerRadius = 6.7
        lblError.isHidden = true
        txtEmail.text = "urja@solulab.com"
        txtPassword.text = "solulab"
    }
    func SendButtonClicked(_ secondDetailViewController: ForgotPasswordViewController) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationFade)
        let emailSentVC = EmailSentViewController(nibName: "EmailSentViewController", bundle: nil)
        
        emailSentVC.view.frame = CGRect.init(x: 18, y: emailSentVC.view.frame.origin.y, width: UIScreen.main.bounds.width - 36, height: emailSentVC.view.frame.height)
        emailSentVC.view.layer.cornerRadius = 5
//        emailSentVC.delegate=self
        self.presentPopupViewController(emailSentVC, animationType: MJPopupViewAnimationFade)
    }
    @IBAction func btnForgotPassword(sender:UIButton) {
        let forgotPasswordVC = ForgotPasswordViewController(nibName: "ForgotPasswordViewController", bundle: nil)
        
        forgotPasswordVC.view.frame = CGRect.init(x: 18, y: forgotPasswordVC.view.frame.origin.y, width: UIScreen.main.bounds.width - 36, height: forgotPasswordVC.view.frame.height)
        forgotPasswordVC.view.layer.cornerRadius = 5

        forgotPasswordVC.delegate=self
        self.presentPopupViewController(forgotPasswordVC, animationType: MJPopupViewAnimationFade)
    }
    @IBAction func btnSignupPress(sender:UIButton) {
        let signupView = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(signupView, animated: true)
    }
    @IBAction func btnLoginPress(sender:UIButton) {
        if validate() {
            let homeView = self.storyboard?.instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController
            self.navigationController?.pushViewController(homeView, animated: true)
        }
    }
    
    func validate() -> Bool {
        if txtEmail.text?.count != 0
                {
            if UtilityClass.emailvalidate(txtEmail.text) {
                txtEmail.hideError()
                lblError.isHidden = true
                if txtPassword.text?.count != 0 {
                    txtEmail.hideError()
                    lblError.isHidden = true
                    return true
                } else {
                    txtEmail.showError()
                    lblError.text = "Please enter password."
                    lblError.isHidden = false
                    return false
                }
            } else {
                txtEmail.showError()
                lblError.text = "Please enter valid email address."
                lblError.isHidden = false

                return false
            }
        } else {
            txtEmail.showError()
            lblError.text = "Please enter email address."
            lblError.isHidden = false
            return false
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

