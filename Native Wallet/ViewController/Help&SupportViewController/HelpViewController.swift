//
//  HelpViewController.swift
//  Native Wallet
//
//  Created by Solulab_Mosam on 03/07/18.
//  Copyright © 2018 Solulab_Mosam. All rights reserved.
//

import UIKit

class HelpViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet var tblObj : UITableView!
    @IBOutlet var tblContact : UITableView!
    @IBOutlet var tblCell : UITableViewCell!
    @IBOutlet var lblFAQ : UILabel!
    @IBOutlet var lblFAQLine : UILabel!
    @IBOutlet var lblContactUs : UILabel!
    @IBOutlet var lblContactUsLine : UILabel!
    var isFAQ : Bool! = true
    var isContactUs : Bool! = false
    @IBOutlet var txtViewMessage : customTextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layerGradient()
        
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Help and Support"
        
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "menu"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        tblObj.tableFooterView = UIView()
        tblContact.tableFooterView = UIView()

        selectFAQ()
        tblContact.isHidden = true
        tblObj.isHidden = false
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblObj
        {
            return 85
        }
        return 600
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblObj
        {
            return 5
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblObj
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! notificationCell
            cell.selectionStyle = .none
            return cell
        }
        else
        {
            return tblCell
        }
    }
    @IBAction func btnFAQPress(sender:UIButton) {
        selectFAQ()
        tblContact.isHidden = true
        tblObj.isHidden = false
        tblObj.reloadData()
    }
    @IBAction func btnContactUsPress(sender:UIButton) {
        selectContactUs()
        tblContact.isHidden = false
        tblObj.isHidden = true
    }
    func selectFAQ() {
        isFAQ = true
        isContactUs = false
        lblFAQ.textColor = GradientColor1
        lblFAQLine.backgroundColor = GradientColor1
        lblContactUs.textColor = GradientColor1.withAlphaComponent(0.7)
        lblContactUsLine.backgroundColor = GradientColor1.withAlphaComponent(0.7)
    }
    func selectContactUs() {
        isFAQ = false
        isContactUs = true
        lblFAQ.textColor = GradientColor1.withAlphaComponent(0.7)
        lblFAQLine.backgroundColor = GradientColor1.withAlphaComponent(0.7)
        lblContactUs.textColor = GradientColor1
        lblContactUsLine.backgroundColor = GradientColor1
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
extension HelpViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "           Message" {
            textView.textColor = UIColor.black
            textView.text = "           "
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "           " {
            textView.textColor = UIColor.lightGray
            textView.text = "           Message"
        }
    }
}
