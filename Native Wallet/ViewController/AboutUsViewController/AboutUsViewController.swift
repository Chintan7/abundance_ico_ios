//
//  AboutUsViewController.swift
//  Native Wallet
//
//  Created by Solulab_Mosam on 03/07/18.
//  Copyright © 2018 Solulab_Mosam. All rights reserved.
//

import UIKit

class AboutUsViewController: BaseViewController
{

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.view.layerGradient()
        
        self.navigationController?.navigationBar.isHidden = false
        self.title = "About Us"
        
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "menu"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

}
