    //
//  TermsandConditionsViewController.swift
//  Native Wallet
//
//  Created by Solulab_Mosam on 29/06/18.
//  Copyright © 2018 Solulab_Mosam. All rights reserved.
//

import UIKit
protocol TermsandConditionsDelegate
{
    func DismissButtonClicked(_ secondDetailViewController: TermsandConditionsViewController)
}
class TermsandConditionsViewController: UIViewController
{
    var delegate: TermsandConditionsDelegate?
    @IBOutlet var lblText : UILabel!
    var strDesc : String!
    var strTitle : String!
    @IBOutlet var lblTitle : UILabel!
    override func viewDidLoad()
    {
        super.viewDidLoad()
//        lblText.text = strDesc
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        lblTitle.text = strTitle
    }
    @IBAction func btnClosePress(sender:UIButton)
    {
        delegate?.DismissButtonClicked(self)
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
