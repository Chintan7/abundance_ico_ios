//
//  customButton.swift
//
//  Created by Ankit on 3/30/16.
//  Copyright © 2016 Ankit. All rights reserved.
//

import UIKit


@IBDesignable
class customButton: UIButton {

    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }

    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect)
    {
        // Drawing code
        
      //  let lineView = UIView(frame: CGRect.init(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height:1))
    //    lineView.backgroundColor=UIColor(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1)
      //  self.addSubview(lineView)
    }
}
